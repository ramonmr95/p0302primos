package net.iescierva.ramonmr95.p0302primos;

import java.util.ArrayList;
import java.util.List;

public class CalcularPrimos {

    List<Integer> listaPrimos = new ArrayList<>();


    public CalcularPrimos() {
        listaPrimos.add(2);
        listaPrimos.add(3);
    }

    public int getPrimo(int posicion) {
        if (posicion == 1) {
            return listaPrimos.get(0);
        }
        else if (posicion == 2) {
            return listaPrimos.get(1);
        }
        else if (posicion < 0) {
            return -1;

        }
        else if (posicion <= listaPrimos.size()) {
            return listaPrimos.get(posicion - 1);
        }
        else {
            int counter = listaPrimos.size();
            int contadorDivisores = 0;
            int num = listaPrimos.get(listaPrimos.size() - 1) + 1;

            while (counter < posicion) {
                for (int i = 2; i < num; i = i + 1) {
                    if (num % i == 0) {
                        contadorDivisores++;
                    }
                }
                if (contadorDivisores == 0) {
                    listaPrimos.add(num);
                    counter++;
                }
                contadorDivisores = 0;
                num = num + 1;
            }
            return listaPrimos.get(listaPrimos.size() - 1);
        }
    }
}
