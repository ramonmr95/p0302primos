package net.iescierva.ramonmr95.p0302primos;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText posicionEditText;
    private TextView primoTextView;
    private CalcularPrimos calcularPrimos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        posicionEditText = findViewById(R.id.posicionEditText);
        primoTextView = findViewById(R.id.primoTextView);
        calcularPrimos = new CalcularPrimos();

    }

    public void calcularPrimo(View view) {
        int posicion = Integer.parseInt(posicionEditText.getText().toString());
        int primo = calcularPrimos.getPrimo(posicion);
        primoTextView.setText(String.valueOf(primo));
    }
}
